<?php

namespace App\Http\Controllers;

use App\Category;
use App\ClientLogo;
use App\Http\Controllers\Controller;
use App\Post;
use App\Role;
use App\SubCategory;
use App\User;
use App\WebsiteLogo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function postList()
    {
        $collection = Post::select('posts.*','users.name as author')
                    ->leftJoin('users','users.id','posts.author_id')
                    ->get()->toArray();
        $data['data'] = $collection;
        return view('Admin.post.post_list',$data);
    }
    public function postCreate()
    {
        $data['category'] = Category::all();
        $data['authors'] = User::all();
        return view('Admin.post.post_create',$data);
    }
    public function getSubcategory(Request $request)
    {
        // dd($request->all());
        try {
            $collection = SubCategory::where('category_id',$request->category_id)->get()->toArray();
            if (count($collection) > 0) {
                return response()->json($collection);
            } else {
                return response()->json([]);
            }
        } catch (\Throwable $th) {
            return response()->json([]);
        }
    }
    public function postCreateAjax(Request $request)
    {
        // dd($request->all());
        // return response()->json(["status" => "success","message" => "Post Saved Successfully"]);

        // if (!$request->rawHtmlContent || !$request->rawTextContent) {
        //     return response()->json(["status" => "error","error" => "Content Field is Required","message" => "Content Field is Required"]);
        // }
        if (!$request->post_id) {
            $validator = request()->validate([
                'post_image'  => 'required|mimes:jpeg,jpg,png,svg|max:2048',
                'post_thumbnail'  => 'required|mimes:jpeg,jpg,png,svg|max:2048',
            ]);
        }
        try {
            // $post = new Post();
            if ($request->post_id) {
                $post = Post::find($request->post_id);
            } else {
                $post = new Post();
            }
            $post->title = $request->title;
            $slug = Str::slug($request->slug, '-');
            $post->slug = $slug;
            $post->meta_description = $request->meta_description;
            $post->author_id = $request->author;
            $post->category_id = $request->category_id;
            $post->subcategory_id = $request->sub_category_id;
            $post->tags = $request->tags;
            $post->is_draft = 0;
            if ($request->is_draft == 'on') {
                $post->is_draft = 1;
            }

            if ($request->post_thumbnail) {
                // $fileName = $request->post_thumbnail->getClientOriginalName();
                $fileName1 = preg_replace('/\s+/', '-', $request->title);
                $fileName1 = time().'_'.$fileName1.'.'.$request->post_thumbnail->extension();
                $filePath1 = $request->file('post_thumbnail')->storeAs('post_thumbnail', $fileName1, 'public');

                $post->post_thumbnail = $filePath1;
            }
            if ($request->post_image) {
                // $fileName = $request->post_thumbnail->getClientOriginalName();
                $fileName2 = preg_replace('/\s+/', '-', $request->title);
                $fileName2 = time().'_'.$fileName2.'.'.$request->post_image->extension();
                $filePath2 = $request->file('post_image')->storeAs('post_image', $fileName2, 'public');
                $post->post_image = $filePath2;
            }
            $post->save();
            return response()->json(["status" => "success","message" => "Post Saved Successfully"]);

        } catch (\Throwable $th) {
            return response()->json(["status" => "error","error" => $th->getMessage(),"message" => $th->getMessage()]);
        }
    }
    public function addEditPostContent($id = null)
    {
        $data['data'] = Post::find($id);
        return view('Admin.post.add_post_content',$data);
    }
    public function addEditPostContentAjax(Request $request)
    {
        try {
            $post = Post::find($request->id);
            $post->content_raw = $request->content_text;
            $post->content_html = $request->content_html;
            $post->save();
            return response()->json(['status' => 'success','message' => 'Post content saved successfully']);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['status' => 'error','message' => $th->getMessage()]);
        }
    }
    public function viewPost($slug = null)
    {
        // $contains = Str::contains($slug, 'admin');
        // dd($contains);
        $post = Post::select('posts.*','users.name as author_name','categories.name as category_name','sub_categories.name as sub_category_name')
                    ->where('slug',$slug)
                    ->leftJoin('categories','categories.id','posts.category_id')
                    ->leftJoin('sub_categories','sub_categories.id','posts.subcategory_id')
                    ->leftJoin('users','users.id','posts.author_id')
                    ->first();
        if ($post) {
            $data['post'] = $post;
            return view('blog_details',$data);
        } else {
            abort(404);
        }

    }
    public function postEdit($id = null)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 4) {
            $data['category'] = Category::all();
            $data['authors'] = User::all();
            $data['post_data'] = Post::find($id);
            $data['subcategory_name'] = SubCategory::find($data['post_data']->subcategory_id)->name;
            if ($data['post_data']) {
                return view('Admin.post.post_edit',$data);
            } else {
                abort(404);
            }
        } else {
            abort(401);
        }

    }
    public function postDelete(Request $request)
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
            try {
                $post = Post::find($request->post_id);
                $post->delete();
                return response()->json(["status" => "success","message" => "Post Deleted Successfully"]);
            } catch (\Throwable $th) {
                return response()->json(["status" => "error","message" => $th->getMessage()]);
            }
        } else {
            abort(401);
        }
    }
    public function usersList()
    {
        if (Auth::user()->role_id == 1) {
            $data['roles'] = Role::all();
            $data['users'] = User::select('users.*','roles.role_name')
                            ->leftJoin('roles','roles.role_id','users.role_id')
                            ->get()->toArray();
            return view('Admin.user.user_list',$data);
        } else {
            abort(401);
        }

    }
    public function userSaveAjax(Request $request)
    {
        // dd($request->all());
        if (Auth::user()->role_id == 1) {
            try {
                if ($request->user_id) {
                    $user = User::find($request->user_id);
                } else {
                    $user = new User();
                }
                $user->name = $request->name;
                $user->email = $request->email;
                $user->role_id = $request->role_id;
                if ($request->password) {
                    $user->password = bcrypt($request->password);
                }
                $user->save();
                return response()->json(["status" => "success","message" => "User saved successfully"]);
            } catch (\Throwable $th) {
                return response()->json(["status" => "error","message" => $th->getMessage()]);
            }
        } else {
            abort(401);
        }
    }
    public function userDelete(Request $request)
    {
        if (Auth::user()->role_id == 1) {
            try {
                $post = User::find($request->user_id);
                $post->delete();
                return response()->json(["status" => "success","message" => "User Deleted Successfully"]);
            } catch (\Throwable $th) {
                return response()->json(["status" => "error","message" => $th->getMessage()]);
            }
        } else {
            abort(401);
        }
    }
    public function websiteLogo()
    {
        $data['data'] = WebsiteLogo::all();
        return view('Admin.website_element.website_logo',$data);
    }
    public function uploadWebsiteLogoAjax(Request $request)
    {
        // dd($request->all());
        try {
            $logo = new WebsiteLogo();
            if ($request->logo) {
                // $fileName = $request->post_thumbnail->getClientOriginalName();
                $fileName1 = 'webiste_logo';
                $fileName1 = time().'_'.$fileName1.'.'.$request->logo->extension();
                $filePath1 = $request->file('logo')->storeAs('website_logo', $fileName1, 'public');
                $logo->path = $filePath1;
            }
            if ($request->is_active && $request->is_active == 'on') {
                $logo->is_active = 1;
            } else {
                $logo->is_active = 0;
            }
            $logo->save();
            return response()->json(["status" => "success","message" => "Logo Uploaded Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status" => "error","message" => $th->getMessage()]);
        }
    }
    public function changeWebsiteLogoStatus(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            if ($request->prev_status == 1) {
                $logo = WebsiteLogo::find($request->logo_id);
                $logo->is_active = 0;
                $logo->save();
            } else {
                // $logo = WebsiteLogo::where('id','!',$request->logo_id);
                DB::table('website_logos')
                    ->where('id','!=', $request->logo_id)
                    ->update([
                        'is_active' => 0
                ]);
                $logo = WebsiteLogo::find($request->logo_id);
                $logo->is_active = 1;
                $logo->save();
            }
            return response()->json(["status" => "success","message" => "Logo Staus Updated Successfully"]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(["status" => "error","message" => $th->getMessage()]);
        }
    }
    public function clientLogo()
    {
        $data['data'] = ClientLogo::all();
        return view('Admin.website_element.client_logo',$data);
    }
    public function uploadClientLogoAjax(Request $request)
    {
        // dd($request->all());
        try {
            $logo = new ClientLogo();
            if ($request->logo) {
                // $fileName = $request->post_thumbnail->getClientOriginalName();
                $fileName1 = 'client_logo';
                $fileName1 = time().'_'.$fileName1.'.'.$request->logo->extension();
                $filePath1 = $request->file('logo')->storeAs('client_logo', $fileName1, 'public');
                $logo->path = $filePath1;
            }
            if ($request->is_active && $request->is_active == 'on') {
                $logo->is_active = 1;
            } else {
                $logo->is_active = 0;
            }
            $logo->save();
            return response()->json(["status" => "success","message" => "Logo Uploaded Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status" => "error","message" => $th->getMessage()]);
        }
    }
    public function changeClientLogoStatus(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $logo = ClientLogo::find($request->logo_id);
            // dd($logo);
            if ($request->prev_status == 1) {
                $logo->is_active = 0;
            }
            if ($request->prev_status == 0) {
                $logo->is_active = 1;
            }
            $logo->save();
            return response()->json(["status" => "success","message" => "Logo Staus Updated Successfully"]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(["status" => "error","message" => $th->getMessage(),"line no" => $th->getLine()]);
        }
    }
    public function categoryList(Request $request)
    {
        $data['data'] = Category::all();
        return view('Admin.post.category_list',$data);
    }
    public function saveCategoryAjax(Request $request)
    {
        // dd($request->all());
        try {
            if ($request->category_id) {
                $category = Category::find($request->category_id);
            } else {
                $category = new Category();
            }
            $category->name = $request->category;
            $category->save();
            return response()->json(["status" => "success","message" => "Category Saved Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status" => "error","message" => $th->getMessage(),"line no" => $th->getLine()]);
        }
    }
    public function deleteCategoryAjax(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $category = Category::find($request->category_id);
            $category->delete();
            // deleting all the subcatgory  realted to category
            $subCategory = SubCategory::where('category_id',$request->category_id)->delete();
            return response()->json(["status" => "success","message" => "Category Deleted Successfully"]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(["status" => "error","message" => $th->getMessage(),"line no" => $th->getLine()]);
        }
    }

    public function subcategoryList(Request $request)
    {
        $data['data'] = SubCategory::select('sub_categories.*','categories.name as category_name')
                                    ->leftJoin('categories','categories.id','sub_categories.category_id')
                                    ->get()->toArray();

        $data['categories'] = Category::all();
        return view('Admin.post.subcategory_list',$data);
    }
    public function saveSubCategoryAjax(Request $request)
    {
        // dd($request->all());
        try {
            if ($request->subcategory_id) {
                $category = SubCategory::find($request->subcategory_id);
            } else {
                $category = new SubCategory();
            }
            $category->name = $request->subcategory;
            $category->category_id = $request->category;
            $category->save();
            return response()->json(["status" => "success","message" => "Sub Category Saved Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status" => "error","message" => $th->getMessage(),"line no" => $th->getLine()]);
        }
    }
    public function deleteSubCategoryAjax(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $subcategory = SubCategory::find($request->subcategory_id);
            $subcategory->delete();

            return response()->json(["status" => "success","message" => "Sub Category Deleted Successfully"]);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(["status" => "error","message" => $th->getMessage(),"line no" => $th->getLine()]);
        }
    }
    public function categoryResult($cid = null)
    {
        try {
            $data['postArr'] = Post::select('posts.*','users.name as author_name','categories.name as category_name','sub_categories.name as sub_category_name')
                    ->where('posts.category_id',$cid)
                    ->where('is_draft',0)
                    ->leftJoin('categories','categories.id','posts.category_id')
                    ->leftJoin('sub_categories','sub_categories.id','posts.subcategory_id')
                    ->leftJoin('users','users.id','posts.author_id')
                    ->get()->toArray();
            try {
                $data['category_name'] = $data['postArr'][0]['category_name'];
            } catch (\Throwable $th) {
                $data['category_name'] = '';
            }
            return view('category_result',$data);
        } catch (\Throwable $th) {
            // dd($th->getMessage());
            abort(500);
        }
    }
}
