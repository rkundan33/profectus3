<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned()->default(0);
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('title');
            $table->longText('content_raw');
            $table->longText('content_html');
            $table->string('post_image');
            $table->string('post_thumbnail');
            $table->longtext('meta_description')->nullable();
            $table->string('layout')->nullable();
            $table->string('tags')->nullable();
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->longText('slug');
            $table->integer('created_by')->nullable();;
            $table->integer('updated_by')->nullable();;
            $table->integer('deleted_by')->nullable();;
            $table->timestamp('deleted_at')->nullable();;
            $table->timestamp('publish_at')->nullable();;
            $table->boolean('is_draft')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
