<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role_name' => 'Super Admin',
            'role_slug' => 'super_admin',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Admin',
            'role_slug' => 'admin',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Moderator',
            'role_slug' => 'moderator',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Writer',
            'role_slug' => 'writer',
        ]);
    }
}
