<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('Reports@123'),
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('Reports@123'),
        ]);

        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Moderator',
            'email' => 'moderator@gmail.com',
            'password' => bcrypt('Reports@123'),
        ]);
        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Writer',
            'email' => 'writer@gmail.com',
            'password' => bcrypt('Reports@123'),
        ]);
    }
}
