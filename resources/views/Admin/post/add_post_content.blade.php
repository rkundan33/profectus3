<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Add Content -Summer Note</title>
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/izitoast/css/iziToast.min.css">
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/izitoast/js/iziToast.min.js"></script>
</head>
<body>
  <div id="summernote"></div>
  <div class="container bg-light">
    <div class="col-md-12 text-center">
        <button type="button" class="btn btn-success" id="contentSave">Save</button>
        <button type="button" class="btn btn-warning" id="reset">Reset</button>
    </div>
</div>
  <script>
    $(document).ready(function() {
        var content_html = `{!!$data->content_html!!}`;
        if (content_html != '') {
            $('#summernote').summernote('code',content_html);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#reset').click(function (e) {
            $('#summernote').summernote('reset');

        });
        $('#contentSave').click(function (e) {
            var html_value = $('#summernote').summernote('code');
            var text_value = $($("#summernote").summernote("code")).text();
            if (html_value == '<p><br></p>' || html_value == '') {
                iziToast.error({
                    title: 'Error!',
                    message: 'Content is required',
                    position: 'topRight'
                });
            } else {
                $.ajax({
                    type: "post",
                    url: "{{url('admin/add-edit-post-content-ajax')}}",
                    data: {
                        content_html : html_value,
                        content_text : text_value,
                        id : '{{$data->id}}'
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            iziToast.success({
                                title: 'Success!',
                                message: response.message,
                                position: 'topRight'
                            });
                            window.setTimeout(function() {
                                window.close();
                            }, 2000);
                        }
                        if (response.status == 'error') {
                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                                position: 'topRight'
                            });
                        }

                    },
                    error: function (error) {
                        iziToast.error({
                            title: 'Error!',
                            message: 'Something went wrong',
                            position: 'topRight'
                        });
                    }
                });
            }

        });
        $('#summernote').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,
            placeholder : 'Post content here',
            focus :  true,
        });
    });
  </script>
</body>
</html>
