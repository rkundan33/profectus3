@extends('layouts.Admin.main')
@section('page_title')
    <title>Cateogry Logo Manage -Admin</title>
@endsection
@section('style_css')
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.css">
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<section class="section">
    <div class="section-body">
        <div class="row">
            <input type="hidden" id="modal-source">
            <input type="hidden" id="user_id">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="d-flex align-items-center mr-auto">Category</h4>

                        <div style="margin-left: 75%">
                            <a class="btn btn-icon icon-left btn-primary add-category">
                                <i class="fas fa-plus"></i>
                                New Category
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="clearfix mb-3"></div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="table-1">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Name</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td> {{$item['name'] }}</td>
                                            <td>{{$item['created_at'] ? date( "d M Y h:i A", strtotime($item['created_at'])) : ''}}</td>

                                            <td>
                                                <div class="button">
                                                    <a  data-id="{{$item['id']}}" data-name="{{$item['name']}}" class="btn btn-icon btn-primary edit-category" target="_blank">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <a  onfocus="this.setAttribute('PrvSelectedValue',this.value);" onclick="if(confirm('Warning - All subcategory realted to this category will be deleted . Are you sure want to delete ?')==false){ this.value=this.getAttribute('PrvSelectedValue');return false;}else{deleteCategory(this);}" data-id="{{$item['id']}}" class="btn btn-icon btn-danger">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{-- user modal start here --}}
    <!-- Modal with form -->
    <div class="modal fade" id="CategoryModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <form id="CategoryForm" class="" autocomplete="off">
                        <div class="form-group">
                            <label>Category</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-layer-group"></i> </div>
                                </div>
                                <input type="text" class="form-control" placeholder="Category Name" name="category" id="category" required>
                            </div>
                        </div>
                        <input type="hidden" id="category_id">
                        <button type="button" class="btn btn-primary m-t-15 waves-effect" id="CategorySaveBtn">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/js/page/datatables.js"></script>
    <script>
        $(document).ready(function () {

            $('.add-category').click(function (e) {
                $('#CategoryModal').appendTo("body").modal('show');
            });
            $("#CategoryModal").on('hide.bs.modal', function(){
                $('#category').val('');
                $('#category_id').val('');
            });
            $('#CategorySaveBtn').click(function (e) {
                var category = $('#category').val();
                if (category == '') {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Please enter category!',
                        position: 'topRight'
                    });
                    return;
                }
                $.ajax({
                    type: "post",
                    url: "{{url('admin/save-category-ajax')}}",
                    data: {
                        category : $('#category').val(),
                        category_id : $('#category_id').val(),
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            $('#CategoryModal').modal('hide');
                            iziToast.success({
                                title: 'Success!',
                                message: response.message,
                                position: 'topRight'
                            });
                            window.setTimeout(function() {
                                window.location.href = '/admin/category-list';
                            }, 2000);
                        }
                        if (response.status == 'error') {
                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                                position: 'topRight'
                            });
                        }
                    },
                    error: function (request, status, error) {
                        iziToast.error({
                            title: 'Error!',
                            message: request.responseJSON.message,
                            position: 'topRight'
                        });
                    }
                });

            });
            $('.edit-category').click(function (e) {
                var category_id = $(this).data('id');
                $('#category_id').val(category_id);
                var category = $(this).data('name');
                $('#category').val(category);
                $('#CategoryModal').appendTo("body").modal('show');

            });
        }); // end of doc ready
        function deleteCategory (data) {
            var category_id = $(data).data('id');
            $.ajax({
                type: "post",
                url: "{{url('admin/delete-category')}}",
                data: {
                    category_id : category_id
                },
                success: function (response) {
                    if (response.status == 'success') {
                        iziToast.success({
                            title: 'Success!',
                            message: response.message,
                            position: 'topRight'
                        });
                        window.setTimeout(function() {
                            window.location.href = '/admin/category-list';
                        }, 2000);
                    }
                    if (response.status == 'error') {
                        iziToast.error({
                            title: 'Error!',
                            message: response.message,
                            position: 'topRight'
                        });
                    }

                },
                error: function (error) {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Something went wrong',
                        position: 'topRight'
                    });
                }
            });
          }
    </script>
@endsection
