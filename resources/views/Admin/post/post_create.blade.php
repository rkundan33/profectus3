@extends('layouts.Admin.main')

@section('page_title')
    <title> Create New Post - Admin</title>
@endsection
@section('style_css')
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/selectize.default.css">
    <style>
        /* .bootstrap-tagsinput {
            min-height: 60px !important;
        } */
        input[type=text].error {
            border: 1px solid red;
        }
        input[type=text].error:focus {
            border: 1px solid red;
        }
        label.error {
            color: red;
        }
    </style>
@endsection
@section('content')
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Write Your Post</h4>
                        </div>
                        <div class="card-body">
                            <form id="postForm" method="POST" enctype="multipart/form-data">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title<sup class="text-danger">*</sup><sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="title" id="tiitle" class="form-control" >

                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Slug<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="slug" >
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Meta Descriiption<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="meta_description">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Author<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <select name="author" id="author" >
                                            <option value="">Select Author</option>
                                            @foreach ($authors as $author)
                                                <option value="{{$author['id']}}">{{$author['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Category<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <select name="category_id" id="select_category" >
                                            <option value="">Please select category</option>
                                            @foreach ($category as $value )
                                                <option value="{{$value['id']}}">{{$value['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Sub Category<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <select name="sub_category_id" id="select_subcategory">
                                            <option value="">Please select subcategory</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Post Image<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <div>
                                            <label for="post_image" >Choose File</label>
                                            <input type="file" name="post_image" id="post_image" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Post Thumbnail<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <div>
                                            <label for="post_thumbnail">Choose File</label>
                                            <input type="file" name="post_thumbnail" id="post_thumbnail" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tags</label>
                                    <div class="col-sm-12 col-md-7">
                                            <input type="text" name="tags" class="form-control" data-role="tagsinput">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    {{-- <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                                <textarea id="ckeditor" name="content" ></textarea>
                                    </div> --}}
                                    <input type="hidden" value="" id="contentrawTextData" name="rawTextContent">
                                    <input type="hidden" value="" id="contentrawHtmlData" name="rawHtmlContent">
                                    <input type="hidden" value="false" id="isFormValid">
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Draft<sup class="text-danger">*</sup></label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="form-check">
                                            <div class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input" id="customCheck2" name="is_draft" checked>
                                              <label class="custom-control-label" for="customCheck2">Check if you don't want to publish!</label>
                                            </div>
                                          </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary" type="button" id="formSave">Create Post</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
        <script src="{{ asset('') }}admin_dashboard/assets/bootstrap-tagsinput.js"></script>
        <script src="{{ asset('') }}admin_dashboard/assets/selectize.js"></script>
        {{-- <script src="{{ asset('') }}admin_dashboard/assets/bundles/ckeditor/ckeditor.js"></script>
        <!-- Page Specific JS File -->
        <script src="{{ asset('') }}admin_dashboard/assets/js/page/ckeditor.js"></script> --}}
        <script src="{{ asset('') }}admin_dashboard/assets/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function () {
                var xhr;
                var select_subcategory, $select_subcategory;
                $("#select_category").selectize({
                    onChange: function(value) {
                        select_subcategory.clear();
                        select_subcategory.clearOptions();
                        select_subcategory.load(function(callback) {
                            xhr && xhr.abort();
                            xhr = $.ajax({
                                url: "{{ route('admin/get-subcategory-by-category-id') }}",
                                data: {
                                    category_id: value
                                },
                                success: function(results) {
                                    //  select_subcategory.enable();
                                    callback(results);
                                },
                                error: function() {
                                    callback();
                                }
                            })
                        });
                    }
                });
                $select_subcategory = $('#select_subcategory').selectize({
                    valueField: 'id',
                    labelField: 'name',
                    searchField: ['name'],
                    onChange: function(value) {
                        // item_table.ajax.reload();
                        // alert('hi');
                    }
                });
                select_subcategory = $select_subcategory[0].selectize;
                $('#author').selectize();

                // post form submit
                $("#postForm").validate({
                    ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input,#ckeditor',
                    rules: {
                        title: {
                            required : true,
                        },
                        slug: {
                            required : true,
                        },
                        meta_description: {
                            required : true,
                        },
                        author: {
                            required : true,
                        },
                        category_id: {
                            required : true,
                        },
                        sub_category_id: {
                            required : true,
                        },
                        post_image: {
                            required : true,
                        },
                        post_thumbnail: {
                            required : true,
                        }

                    },
                    messages: {
                        // title: "Title is required",
                        // lastname: "Enter your lastname",
                        // username: {
                        //     : "Enter a username",
                        //     minlength: jQuery.format("Enter at least {0} characters"),
                        //     remote: jQuery.format("{0} is already in use")
                        // }
                    },
                    submitHandler: function (form) {
                        //form[0].submit(); // submit the form
                        $('#isFormValid').val('true');
                    }
                });
                $('#formSave').click(function (e) {
                    $('#postForm').submit();

                });
                $('#postForm').submit(function (e) {
                    // console.log(e);
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#isFormValid').val() == 'true') {
                        $.ajax({
                            type: "POST",
                            url: "{{ url('admin/post-new-ajax')}}",
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            beforeSend: function() {
                                $("#overlay").fadeIn();　
                            },
                            success: function (response) {
                                if (response.status == 'success') {
                                    iziToast.success({
                                        title: 'Success!',
                                        message: response.message,
                                        position: 'topRight'
                                    });
                                    window.setTimeout(function() {
                                        window.location.href = '/admin/post';
                                    }, 3000);

                                }
                                if (response.status == 'error') {
                                    iziToast.error({
                                        title: 'Error!',
                                        message: response.message,
                                        position: 'topRight'
                                    });
                                }
                            },
                            error: function (request, status, error) {
                                iziToast.error({
                                    title: 'Error!',
                                    message: request.responseJSON.message,
                                    position: 'topRight'
                                });
                            },
                            complete: function() {
                                $("#overlay").fadeOut();　
                            }
                        });
                    } else {
                        iziToast.error({
                            title: 'Error!',
                            message: 'Please Fill All The Required Filed',
                            position: 'topRight'
                        });
                    }
                });
            });
        </script>
@endsection
