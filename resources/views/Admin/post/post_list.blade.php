@extends('layouts.Admin.main')
@section('page_title')
<title>Post Create - Admin</title>
@endsection
@section('style_css')
<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="d-flex align-items-center mr-auto">All Posts</h4>

                            <div style="margin-left: 80%">
                                <a href="/admin/new-post" target="_blank" class="btn btn-icon icon-left btn-primary">
                                    <i class="fas fa-plus"></i>
                                    New Post
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="clearfix mb-3"></div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th style="max-width: 120px">Title</th>
                                            <th>Post Image</th>
                                            <th>Tags</th>
                                            <th>Status</th>
                                            <th >Action</th>
                                            <th style="min-width: 111px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $key =>$item)
                                            <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$item['title']}}</td>
                                                    {{-- <td>{{$item['author']}}</td> --}}
                                                    <td>
                                                        <a href="#">

                                                            <img alt="image" src="{{ asset('storage/'.$item['post_image'])}}"
                                                                class="square-circle" width="93" data-bs-toggle="title" title="">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        @php
                                                            $tags = $item['tags'];
                                                            $tags_arr = explode (",", $tags);
                                                        @endphp
                                                        @foreach ($tags_arr as $tag)
                                                            <span class="badge badge-info">{{$tag}}</span>
                                                        @endforeach
                                                    </td>
                                                    <td>@if ($item['is_draft'] == 0)
                                                            <div class="badge btn-success">Published</div>
                                                        @elseif ($item['is_draft'] == 1)
                                                            <div class="badge btn-warning">Draft</div>

                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="/admin/add-edit-post-content/{{$item['id']}}" class="btn btn-icon btn-primary bt-sm" target="_blank">
                                                            Post Content
                                                        </a>
                                                    </td>
                                                    <td >
                                                        <div class="button">
                                                            <a href="/{{$item['slug']}}" class="btn btn-icon btn-success" target="_blank">
                                                                <i class="fas fa-eye"></i>
                                                            </a>
                                                            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 4)
                                                                <a href="/admin/post-edit/{{$item['id']}}" class="btn btn-icon btn-primary" target="_blank">
                                                                    <i class="far fa-edit"></i>
                                                                </a>
                                                            @endif
                                                            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                                                <a  onfocus="this.setAttribute('PrvSelectedValue',this.value);" onclick="if(confirm('Are you sure want to delete ?')==false){ this.value=this.getAttribute('PrvSelectedValue');return false;}else{deletePost(this);}" data-id="{{$item['id']}}" class="btn btn-icon btn-danger">
                                                                    <i class="far fa-trash-alt"></i>
                                                                </a>
                                                            @endif

                                                        </div>
                                                    </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
<script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.js"></script>
<script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('') }}admin_dashboard/assets/js/page/datatables.js"></script>
<script>
    $(document).ready(function () {

    }); // end of doc ready
    function deletePost(data) {
        var post_id = $(data).data('id');
        $.ajax({
            type: "post",
            url: "{{url('admin/delete-post')}}",
            data: {
                post_id : post_id
            },
            success: function (response) {
                if (response.status == 'success') {
                    iziToast.success({
                        title: 'Success!',
                        message: response.message,
                        position: 'topRight'
                    });
                    window.setTimeout(function() {
                        window.location.href = '/admin/post';
                    }, 2000);
                }
                if (response.status == 'error') {
                    iziToast.error({
                        title: 'Error!',
                        message: response.message,
                        position: 'topRight'
                    });
                    window.setTimeout(function() {
                        window.location.href = '/admin/post';
                    }, 2000);
                }

            },
            error: function (error) {
                iziToast.error({
                    title: 'Error!',
                    message: 'Something went wrong',
                    position: 'topRight'
                });
                window.setTimeout(function() {
                    window.location.href = '/admin/post';
                }, 2000);
            }
        });
    }
</script>
@endsection
