@extends('layouts.Admin.main')
@section('page_title')
    <title>Sub Cateogry-Admin</title>
@endsection
@section('style_css')
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.css">
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<section class="section">
    <div class="section-body">
        <div class="row">
            <input type="hidden" id="modal-source">
            <input type="hidden" id="user_id">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="d-flex align-items-center mr-auto">Sub Category</h4>

                        <div style="margin-left: 75%">
                            <a class="btn btn-icon icon-left btn-primary add-sub-category">
                                <i class="fas fa-plus"></i>
                                New
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="clearfix mb-3"></div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="table-1">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Subcategory Name</th>
                                        <th>Category Name</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td> {{$item['name'] }}</td>
                                            <td> {{$item['category_name'] }}</td>
                                            <td>{{$item['created_at'] ? date( "d M Y h:i A", strtotime($item['created_at'])) : ''}}</td>

                                            <td>
                                                <div class="button">
                                                    <a  data-id="{{$item['id']}}" data-name="{{$item['name']}}" data-category="{{$item['category_id']}}" class="btn btn-icon btn-primary edit-sub-category" target="_blank">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <a  onfocus="this.setAttribute('PrvSelectedValue',this.value);" onclick="if(confirm('Are you sure want to delete ?')==false){ this.value=this.getAttribute('PrvSelectedValue');return false;}else{deleteSubCategory(this);}" data-id="{{$item['id']}}" class="btn btn-icon btn-danger">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{-- user modal start here --}}
    <!-- Modal with form -->
    <div class="modal fade" id="SubCategoryModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Add Sub Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <form id="CategoryForm" class="" autocomplete="off">
                        <div class="form-group">
                            <label>Sub Category</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-layer-group"></i> </div>
                                </div>
                                <input type="text" class="form-control" placeholder="Sub Category Name" name="subcategory" id="subcategory" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-layer-group"></i> </div>
                                </div>
                                <select name="category" id="category" class="form-control">
                                    <option selected disabled>Please select category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{$category['id']}}">{{$category['name']}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="subcategory_id">
                        <button type="button" class="btn btn-primary m-t-15 waves-effect" id="SubCategorySaveBtn">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/js/page/datatables.js"></script>
    <script>
        $(document).ready(function () {

            $('.add-sub-category').click(function (e) {
                $('#SubCategoryModal').appendTo("body").modal('show');
            });
            $("#SubCategoryModal").on('hide.bs.modal', function(){
                $('#subcategory').val('');
                $('#subcategory_id').val('');
                $('#category').val('');
            });
            $('#SubCategorySaveBtn').click(function (e) {
                var subcategory = $('#subcategory').val();
                var category = $('#category').val();
                // console.log(category);
                if (subcategory == '' || category == '' || category == null ) {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Please enter sub category!',
                        position: 'topRight'
                    });
                    return;
                }
                $.ajax({
                    type: "post",
                    url: "{{url('admin/save-sub-category-ajax')}}",
                    data: {
                        subcategory : $('#subcategory').val(),
                        subcategory_id : $('#subcategory_id').val(),
                        category : $('#category').val()
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            $('#SubCategoryModal').modal('hide');
                            iziToast.success({
                                title: 'Success!',
                                message: response.message,
                                position: 'topRight'
                            });
                            window.setTimeout(function() {
                                window.location.href = '/subcategory-list';
                            }, 2000);
                        }
                        if (response.status == 'error') {
                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                                position: 'topRight'
                            });
                        }
                    },
                    error: function (request, status, error) {
                        iziToast.error({
                            title: 'Error!',
                            message: request.responseJSON.message,
                            position: 'topRight'
                        });
                    }
                });

            });
            $('.edit-sub-category').click(function (e) {
                var subcategory_id = $(this).data('id');
                $('#subcategory_id').val(subcategory_id);
                var subcategory = $(this).data('name');
                var category = $(this).data('category');
                $('#subcategory').val(subcategory);
                $('#category').val(category)
                $('#SubCategoryModal').appendTo("body").modal('show');

            });
        }); // end of doc ready
        function deleteSubCategory (data) {
            var subcategory_id = $(data).data('id');
            $.ajax({
                type: "post",
                url: "{{url('admin/delete-sub-category')}}",
                data: {
                    subcategory_id : subcategory_id
                },
                success: function (response) {
                    if (response.status == 'success') {
                        iziToast.success({
                            title: 'Success!',
                            message: response.message,
                            position: 'topRight'
                        });
                        window.setTimeout(function() {
                            window.location.href = '/admin/subcategory-list';
                        }, 2000);
                    }
                    if (response.status == 'error') {
                        iziToast.error({
                            title: 'Error!',
                            message: response.message,
                            position: 'topRight'
                        });
                    }

                },
                error: function (error) {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Something went wrong',
                        position: 'topRight'
                    });
                }
            });
          }
    </script>
@endsection
