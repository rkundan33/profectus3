@extends('layouts.Admin.main')
@section('page_title')
    <title>Users List -Admin</title>
@endsection
@section('style_css')
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.css">
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<section class="section">
    <div class="section-body">
        <div class="row">
            <input type="hidden" id="modal-source">
            <input type="hidden" id="user_id">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="d-flex align-items-center mr-auto">Users List</h4>

                        <div style="margin-left: 80%">
                            <a class="btn btn-icon icon-left btn-primary user-modal">
                                <i class="fas fa-plus"></i>
                                New User
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="clearfix mb-3"></div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="table-1">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Ful Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $key => $user)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$user['name']}}</td>
                                            <td>{{$user['email']}}</td>
                                            <td>{{$user['role_name']}}</td>
                                            <td>{{$user['created_at'] ? date( "d M Y h:i A", strtotime($user['created_at'])) : ''}}</td>
                                            <td>
                                                <div class="button">
                                                    <a data-id="{{$user['id']}}" data-name="{{$user['name']}}" data-email="{{$user['email']}}" data-role="{{$user['role_id']}}" class="btn btn-icon btn-primary user-edit">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <a  onfocus="this.setAttribute('PrvSelectedValue',this.value);" onclick="if(confirm('Are you sure want to delete ?')==false){ this.value=this.getAttribute('PrvSelectedValue');return false;}else{deleteUser(this);}" data-id="{{$user['id']}}" class="btn btn-icon btn-danger">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{-- user modal start here --}}
    <!-- Modal with form -->
    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Create User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <form class="" autocomplete="off">
                        <div class="form-group">
                            <label>Full Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-user"></i> </div>
                                </div>
                                <input type="text" class="form-control" placeholder="Full Name" name="name" id="name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-envelope"></i> </div>
                                </div>
                                <input type="email" class="form-control" placeholder="Email" name="email" id="email" autocomplete="nope" required> </div>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-user-secret"></i> </div>
                                </div>
                                <select name="role_id" id="role_id" class="form-control">
                                    <option value="" selected disabled>Select role</option>
                                        @foreach ($roles as $role)
                                            <option value="{{$role['role_id']}}">{{$role['role_name']}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Password <span class="d-none password-hint">(Leave password blank if you don't want to update !) </span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fas fa-lock"></i> </div>
                                </div>
                                <input type="password"  class="form-control" placeholder="Password" name="password" id="password" autocomplete="new-password"> </div>
                        </div>
                        <button type="button" class="btn btn-primary m-t-15 waves-effect" id="userSaveBtn">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/js/page/datatables.js"></script>
    <script>
        $(document).ready(function () {
            $('.user-modal').click(function (e) {
                $('#modal-source').val('create');
                $('#userModal').appendTo("body").modal('show');
            });
            $("#userModal").on('hide.bs.modal', function(){
                $('#name').val('');
                $('#email').val('');
                $('#role_id').val('');
                $('#password').val('');
                $('#modal-source').val('');
                $('#user_id').val('');
                $('.password-hint').removeClass('d-none').addClass('d-none');
            });
            $('#userSaveBtn').click(function (e) {
                var name = $('#name').val();
                var email = $('#email').val();
                var role_id = $('#role_id').val();
                var password = $('#password').val();
                if ($('#modal-source').val() == 'create') {
                    if (name == '' || email == '' || role_id == '' || password == '') {
                        iziToast.error({
                            title: 'Error!',
                            message: 'All fields are required!',
                            position: 'topRight'
                        });
                        return;
                    }
                }
                if ($('#modal-source').val() == 'edit') {
                    if (name == '' || email == '' || role_id == '') {
                        iziToast.error({
                            title: 'Error!',
                            message: 'All fields are required!',
                            position: 'topRight'
                        });
                        return;
                    }
                }
                $.ajax({
                    type: "post",
                    url: "{{url('admin/save-user-ajax')}}",
                    data:  {
                        name : name,
                        email : email,
                        role_id : role_id,
                        password : password,
                        user_id : $('#user_id').val()
                    },
                    success: function (response) {
                        if (response.status == 'success') {
                            $('#userModal').modal('hide');
                            iziToast.success({
                                title: 'Success!',
                                message: response.message,
                                position: 'topRight'
                            });
                            window.setTimeout(function() {
                                window.location.href = '/users-list';
                            }, 2000);
                        }
                        if (response.status == 'error') {
                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                                position: 'topRight'
                            });
                        }
                    },
                    error: function (request, status, error) {
                        iziToast.error({
                            title: 'Error!',
                            message: request.responseJSON.message,
                            position: 'topRight'
                        });
                    }
                });

            });
            $('.user-edit').click(function (e) {
                $('.password-hint').removeClass('d-none');
                $('#modal-source').val('edit');
                var user_id = $(this).data('id');
                $('#user_id').val(user_id);
                var name = $(this).data('name');
                var email = $(this).data('email');
                var role_id = $(this).data('role');
                $('#name').val(name);
                $('#email').val(email);
                $('#role_id').val(role_id);
                $('#userModal').appendTo("body").modal('show');
            });
        }); // end of doc ready
        function deleteUser (data) {
            var user_id = $(data).data('id');
            $.ajax({
                type: "post",
                url: "{{url('admin/delete-user')}}",
                data: {
                    user_id : user_id
                },
                success: function (response) {
                    if (response.status == 'success') {
                        iziToast.success({
                            title: 'Success!',
                            message: response.message,
                            position: 'topRight'
                        });
                        window.setTimeout(function() {
                            window.location.href = '/admin/users-list';
                        }, 2000);
                    }
                    if (response.status == 'error') {
                        iziToast.error({
                            title: 'Error!',
                            message: response.message,
                            position: 'topRight'
                        });
                        window.setTimeout(function() {
                            window.location.href = '/admin/users-list';
                        }, 2000);
                    }

                },
                error: function (error) {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Something went wrong',
                        position: 'topRight'
                    });
                    window.setTimeout(function() {
                        window.location.href = '/admin/users-list';
                    }, 2000);
                }
            });
          }
    </script>
@endsection
