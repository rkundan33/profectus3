@extends('layouts.Admin.main')
@section('page_title')
    <title>Client Logo Manage -Admin</title>
@endsection
@section('style_css')
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.css">
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<section class="section">
    <div class="section-body">
        <div class="row">
            <input type="hidden" id="modal-source">
            <input type="hidden" id="user_id">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="d-flex align-items-center mr-auto">Client Logo</h4>

                        <div style="margin-left: 70%">
                            <a class="btn btn-icon icon-left btn-primary logo-modal">
                                <i class="fas fa-plus"></i>
                                New Logo
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="clearfix mb-3"></div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="table-1">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Logo</th>
                                        <th>Created At</th>
                                        <th>Status</th>
                                        <th>Change Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>

                                                <a href="{{ asset('storage/'.$item['path'])}}" target="_blank">

                                                    <img alt="image" src="{{ asset('storage/'.$item['path'])}}"
                                                        class="square-circle" width="120" data-bs-toggle="title" title="">
                                                </a>
                                            </td>
                                            <td>{{$item['created_at'] ? date( "d M Y h:i A", strtotime($item['created_at'])) : ''}}</td>
                                            <td>
                                                @if($item['is_active'] == 1)
                                                <a href="#" class="badge badge-success">Active</a>
                                                @endif
                                                @if($item['is_active'] == 0)
                                                <a href="#" class="badge btn-warning">Deactive</a>
                                                @endif
                                            </td>

                                            <td class="text-center">
                                                <div class="button">
                                                    @if($item['is_active'] == 0)
                                                        <a  onfocus="this.setAttribute('PrvSelectedValue',this.value);" onclick="if(confirm('Are you sure want to change status ?')==false){ this.value=this.getAttribute('PrvSelectedValue');return false;}else{activeLogo(this);}" data-id="{{$item['id']}}" data-active="{{$item['is_active']}}" class="btn btn-icon btn-success">
                                                            <i class="fas fa-check "></i>
                                                        </a>
                                                    @endif
                                                    @if($item['is_active'] == 1)
                                                        <a  onfocus="this.setAttribute('PrvSelectedValue',this.value);" onclick="if(confirm('Are you sure want to change status ?')==false){ this.value=this.getAttribute('PrvSelectedValue');return false;}else{activeLogo(this);}" data-id="{{$item['id']}}" data-active="{{$item['is_active']}}" class="btn btn-icon btn-danger">
                                                            <i class="fas fa-times "></i>
                                                        </a>
                                                    @endif

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{-- user modal start here --}}
    <!-- Modal with form -->
    <div class="modal fade" id="logoModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Upload Logo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <form id="logoForm" class="" autocomplete="off">
                        <div class="form-group">
                            <label>Upload Logo</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="far fa-file-image"></i> </div>
                                </div>
                                <input type="file" class="form-control" placeholder="Upload logo" name="logo" id="logo" required>
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" name="is_active" class="custom-control-input" id="is_active">
                              <label class="custom-control-label" for="remember-me">Active</label>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary m-t-15 waves-effect" id="logoSaveBtn">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/datatables.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('') }}admin_dashboard/assets/js/page/datatables.js"></script>
    <script>
        $(document).ready(function () {

            $('.logo-modal').click(function (e) {
                $('#logoModal').appendTo("body").modal('show');
            });
            $('#logoSaveBtn').click(function (e) {
                var logo = $('#logo').val();
                if (logo == '') {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Please select logo!',
                        position: 'topRight'
                    });
                    return;
                }
                $('#logoForm').submit();

            });
            $('#logoForm').submit(function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "post",
                    url: "{{url('admin/upload-client-logo-ajax')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 'success') {
                            $('#logoModal').modal('hide');
                            iziToast.success({
                                title: 'Success!',
                                message: response.message,
                                position: 'topRight'
                            });
                            window.setTimeout(function() {
                                window.location.href = '/admin/website/client-logo';
                            }, 2000);
                        }
                        if (response.status == 'error') {
                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                                position: 'topRight'
                            });
                        }
                    },
                    error: function (request, status, error) {
                        iziToast.error({
                            title: 'Error!',
                            message: request.responseJSON.message,
                            position: 'topRight'
                        });
                    }
                });
            });
        }); // end of doc ready
        function activeLogo (data) {
            var logo_id = $(data).data('id');
            var prev_status = $(data).data('active');
            $.ajax({
                type: "post",
                url: "{{url('admin/change-client-logo-status')}}",
                data: {
                    logo_id : logo_id,
                    prev_status : prev_status,
                },
                success: function (response) {
                    if (response.status == 'success') {
                        iziToast.success({
                            title: 'Success!',
                            message: response.message,
                            position: 'topRight'
                        });
                        window.setTimeout(function() {
                            window.location.href = '/admin/website/client-logo';
                        }, 2000);
                    }
                    if (response.status == 'error') {
                        iziToast.error({
                            title: 'Error!',
                            message: response.message,
                            position: 'topRight'
                        });
                    }

                },
                error: function (error) {
                    iziToast.error({
                        title: 'Error!',
                        message: 'Something went wrong',
                        position: 'topRight'
                    });
                }
            });
          }
    </script>
@endsection
