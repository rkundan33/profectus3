@extends('layouts.main')
@section('title')
@yield('title', 'About Us | Profectus Magazine')@endsection

@section('meta')
<!-- Primary Meta Tags -->

<meta name="title" content="About Us | Profectus Magazine">
<meta name="description" content="Each story we feature at Profectus comes with a perspective, a perspective of better tomorrow whether it be an individual or a company, whether it be social world or business, or of an average person, we at Profectus believe in telling stories that push you towards taking an initiative and making India bette with creative ideas and innovation.">
<link rel="canonical" href="{{ Request::url()}}" />
<!-- Open Graph / Facebook -->
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article">
<meta property="og:url" content="{{ Request::url()}}">
<meta property="og:title" content="About Us via @ProfectusMagazine ">
<meta property="og:description" content="Each story we feature at Profectus comes with a perspective, a perspective of better tomorrow whether it be an individual or a company, whether it be social world or business, or of an average person, we at Profectus believe in telling stories that push you towards taking an initiative and making India bette with creative ideas and innovation.">
<meta property="og:site_name" content="ProfectusMagazine" />
<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="{{ Request::url()}}">
<meta property="twitter:title" content="About Us | Profectus Magazine">
    
@endsection
@section('content')


<div class="container">
    <center><img src="https://profectusmagazine.com/wp-content/uploads/2021/03/about-us-696x220.png" width="220px" style="margin-bottom: 30px;"></center>
    <div class="row align-items-center justify-content-center">
      <div class="col-12 col-lg-6">
        <div class="welcome-thumb thumb-animated"> <img src="https://www.susangreenecopywriter.com/wp-content/uploads/2013/01/photo-1518081461904-9d8f136351c2.jpg" width="100%" alt="" style="border-radius: 30px;box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;"> </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="service-text pt-4 pt-lg-0">
          <center>
            <p style="font-size: 17px;">Each story we feature at <b style="color:#ff6600;">Profectus</b> comes with a perspective, a perspective of better tomorrow whether it be a story of individual or a Brand, whether it be social world or business, or of a startup or well established business, we at <b style="color:#ff6600;">Profectus</b> believe in publishing stories that create brand reputation by connecting audience through real life journey of Entrepreneurs, Startups and esteemed organizations or a brands. Stories that motivate in taking initiative and making India better with creative ideas and innovation. Our Pride of the nation and Unsung heroes segment brings the stories of Social Worker and Individual that’ll inspire audience in doing humanitarian work, and making this world a better place. </p>
          </center>
        </div>
      </div>
    </div>
  </div>
  <div class="container" style="margin-top: 80px;">
    <center><img src="https://profectusmagazine.com/wp-content/uploads/2021/11/What-We-offer-300x38.png" style="margin-bottom: 30px;"></center>
    <div class="row align-items-center justify-content-center">
      <div class="col-12 col-lg-12">
        <center>
          <h2 style="margin-bottom: 30px;"><i><u>BRAND STORY FOR <b style="color:#ff6600;">PR</b></u></i></h2></center>
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-12 col-lg-6">
              <div class="service-text pt-4 pt-lg-0">
                <p style="font-weight: 600;"> It is always important for an organization to win the confidence of the public and have a positive image in the market to carve a niche for itself. Even if the quality of all your offerings is top notch, you will never get the desired traction if you are not able to establish a connection with your public. This is where the magic of Brand Story comes in!</p>
                <p style="font-weight: 600;">We are a reputed Publisher in India, dedicated to ensuring that your brand image never falters. As a top Publishing house in the country, we focus more on sharing your brand success story instead of “ Buy this Product” We understand the essence of your brand communicates with the public for stimulating their opinion in the most organic way possible. </p>
              </div>
            </div>
            <div class="col-12 col-lg-6">
              <div class="welcome-thumb thumb-animated"> <img src="https://blog.fintechgie.com/wp-content/uploads/2019/12/pubrelf.jpg" width="100%" alt="" style="border-radius: 30px;box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;"> </div>
            </div>
          </div>
        </div>
        <p>
          <h5 style="color:#ff6600;"><b>What differentiate us from others?</b></h5>
          <ul style="font-weight: 600;">
            <li>We conduct Interview with Founders to know the real story of the brand inception</li>
            <li>We write Articles based on interviews and conversation with founders (No internet researched article or Media kit Article from Brands)</li>
            <li>Experienced Journalists & Editorial team to pen down your brand story for creating brand awareness to Customers, Stakeholders, Employees and Investors.</li>
            <li>Printed article in Magazine for lifetime access for audience</li>
            <li>PAN India Circulation.</li>
            <li>10K + Subscribers within 9 months for Print & Digital Magazine</li>
            <li>250K+ Post reach on Social Media Platforms (Instagram, Twitter & Facebook)</li>
            <li>Lifetime access to Article on Website + Social Media + Print Magazine + PDF of Article</li>
            <li>150+ Venture Capitalists & Angel Investors in our Subscriber list.</li>
          </ul>
        </p>
        <div class="feat bg-gray pt-5 pb-5">
          <div class="container">
            <div class="row">
              <div class="section-head col-sm-12" style="padding-bottom: 40px;">
                <center>
                  <h2><span>Advantages of <b style="color:#ff6600;">BRAND STORY</b></span></h2></center>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="item">
                  <h6 style="color:#ff6600;"><b>POSITIVE BRAND IMAGE</b></h6>
                  <p style="font-weight: 600;">Brand Story is important to create and maintain a positive image in the minds of the public. Whether you are a Startup, Successful Entrepreneur, MSME, MNC or Established Brand, public opinion will always hold the power to influence your actions, worth and reputation in the market. Businesses are often troubled with their “core messages” not being propagated to the customers and other stakeholders. Advertising does only half the job as it asks people to buy your products or avail of your services and that is it. It usually does not have the bandwidth to go beyond that.</p>
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="item">
                  <h6 style="color:#ff6600;"><b>IMPORTANCE</b></h6>
                  <p style="font-weight: 600;">You cannot survive in a market for long without having a favorable image in the public. Whether it is the internal public (employees, partners, investors, etc) or external public (customers, suppliers, clients, etc), Brand story help you create a positive image in their minds, which can produce fruitful results for your brand. We write Brand story, starting from its inception to execution and success to create and circulate a positive image of a brand in the minds of the public (Internal & External). Most Brand story helps in building brand reputation and to attract/retain more customers.</p>
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="item">
                  <h6 style="color:#ff6600;"><b>DIFFERENCE</b></h6>
                  <p style="font-weight: 600;">Brand Story is the process of creating a desirable image of a brand in the minds of the public, helping them manage crises and the reputation they hold in the market. It is a more wholesome process that may involve digital marketing but it is more of personal store to convey “Core message” of brand to audience. On the other hand, digital marketing is the process of creating awareness about a brand across all the digital platforms with the sole aim of making conversions. Digital marketing is not really concerned about the image and reputation of the brand. Its ultimate aim is lead conversion.</p>
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="item">
                  <h6 style="color:#ff6600;"><b>KICK STARTER FOR STARTUPS</b></h6>
                  <p style="font-weight: 600;">If you have just entered the market with a startup and are looking for seed funding from sources like Venture capitalists, Angel Investors, or even commercial banks, the reputation that your business holds plays a very important role in getting the finances.</p>
                </div>
              </div>
              <div class="col-lg-4 col-sm-6">
                <div class="item">
                  <h6 style="color:#ff6600;"><b>IMPACT</b></h6>
                  <p style="font-weight: 600;">Brand Story does not focus on hard selling. It goes beyond the transactional nature of marketing and focuses on the core values of your brand. Once your audience is hooked to the core value of your brand, you are bound to make more sales!</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container" style="margin-top: 30px;">
    <div class="row align-items-center justify-content-center">
      <div class="col-12 col-lg-12">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-12 col-lg-12">
              <center>
                <h2 style="margin-bottom: 30px;"><i><u><b style="color:#ff6600;">PRINT ADVERTISEMENT</b></u></i></h2></center>
              <div class="container">
                <div class="row align-items-center justify-content-center">
                  <div class="col-12 col-lg-6">
                    <div class="welcome-thumb thumb-animated"> <img src="https://chapeltonboard.com/wp-content/uploads/2018/10/shutterstock_355917371-magazines-on-shelf.jpg" width="100%" alt="" style="border-radius: 30px;box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;"> </div>
                  </div>
                  <div class="col-12 col-lg-6">
                    <div class="service-text pt-4 pt-lg-0">
                      <p style="font-weight: 600;"> Print media has been the number one advertising method for centuries – yet no new media has been able to challenge the still-rising popularity of print media among its consumers as well as among wise advertisers and marketers. The main reason for this is that print media is one of the most trusted ways of communication and one of the cheapest ways to reach a broad audience fast.</p>
                      <p style="font-weight: 600;">However, with digital media marketing becoming the new trend in marketing, many marketers forget print media. But marketers and advertisers should not overlook the power of print media even during this digital age.</p>
                      <p style="font-weight: 600;">Thus, we at Profectus offer most reliable advertisement for you to decide on whether you need to incorporate print media in your future marketing efforts or not. </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="feat bg-gray pt-5 pb-5">
            <div class="container">
              <div class="row">
                <div class="section-head col-sm-12" style="padding-bottom: 40px;">
                  <center>
                    <h2><span>Advantages of <b style="color:#ff6600;">PRINT ADVERTISING</b></h2></span>
                  </center>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>APPEALING VISUALIZATION</b></h6>
                    <p style="font-weight: 600;">Profectus magazine use high quality images and paper material that allows best representation of your product. Flashy images in profectus magazine get the attention of many readers. Thus if you have a killer ad design, the results are always going to be promising.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>FOCUSED ATTENTION</b></h6>
                    <p style="font-weight: 600;">Printed materials capture the attention of the reader. If your audience is physically holding a copy of a Profectus magazine, they don’t have the opportunity to multitask on multiple forms of media. It is more likely that you will have their complete attention.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>NO COMPETITION</b></h6>
                    <p style="font-weight: 600;">We practice “No Competition Policy” by not advertising same or similar Product/Service in a single edition of our quarterly magazine, which gives your advertisement higher chance of conversion. Profectus Magazine provides 100% justification and value to our prestigious Advertisers ROAS.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>HIGHER ROI THAN DIGITAL ADS</b></h6>
                    <p style="font-weight: 600;">There is a high tendency for print media to keep at homes and re-read and shared. Profectus magazines at the doctor’s office, the library or public places, at the reception desk of hotels or office have a long shelf life which increases your ad’s exposure. Following gives print ads higher chances to stick than digital ads which forget instantly.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>ESTABLISHING TRUST</b></h6>
                    <p style="font-weight: 600;">Our Researches shows that print media is one of the most trusted media outlets out there. A survey revealed 87% of participants trusted printed advertisements published in Magazine when making a purchase decision. Since people purchase from those who they trust, advertising in Profectus Magazine can attract you more sales than any other media outlets.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>READER CONFIDENCE</b></h6>
                    <p style="font-weight: 600;">Fake news and cybercrimes have made many people hesitant about online engagement. Profectus Magazine has a large loyal customer base that has subscribed to receive the publication on every Quarterly. Advertisement in Profectus Magazine is published after certain verifications, which gives audience a confidence to make a call or visit you to avail your offerings. Thus you are assured of massive exposure for your advertisement with print media</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>DURABILITY</b></h6>
                    <p style="font-weight: 600;">Unlike online content, Profectus magazine are more likely to leave long lasting impression because they are a physical object (Printed Magazine). As consumers hold onto a Profectus magazine for an extended period of time, an advertisement continues working each time they pick up the issue. People will even save images and pages for inspiration or trade magazine issues with friends. Since Profectus magazine has durability, contents and advertisement published in it leaves a lasting message.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>FLEXIBILITY</b></h6>
                    <p style="font-weight: 600;">Profectus Magazine Print advertising has flexible Quarterly plan and Yearly plan for full-page advertisement. It gives Advertisers the flexibility in selecting the type of ad to fit their requirements and budget.</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                  <div class="item">
                    <h6 style="color:#ff6600;"><b>REACH MULTIPLE GENERATIONS</b></h6>
                    <p style="font-weight: 600;">Readers of Profectus magazines are in range from teens, young adults to senior citizens, enabling you to target a wide range of audience easily.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    <center><img src="https://profectusmagazine.com/wp-content/uploads/2021/09/our-happy-client-png-696x220.png" width="320px" style="margin-bottom:-50px;"></center>
    
    @include('client_logo_section')
@endsection