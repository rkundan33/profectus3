@extends('layouts.main')
@section('title'){{$post['title']}}@endsection
@section('meta')
<!-- Primary Meta Tags -->
<meta name="title" content="{{$post['title']}} | Profectus Magazine">
<meta name="description" content="{!!$post->meta_description!!}">
<link rel="canonical" href="{{ Request::url()}}" />
<!-- Open Graph / Facebook -->
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article">
<meta property="og:url" content="{{ Request::url()}}">
<meta property="og:title" content="{{$post['title']}} via @ProfectusMagazine ">
<meta property="og:description" content="{!!$post->meta_description!!}">
<meta property="og:image" content="{{ asset('storage/'.$post->post_image)}}">
<meta property="og:site_name" content="ProfectusMagazine" />
<meta property="article:published_time" content="{{$post->created_at ? date( "d-M-Y h:i:sa", strtotime($post->created_at)) : ''}}" />

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="{{ Request::url()}}">
<meta property="twitter:title" content="{{$post['title']}}| Profectus Magazine">
<meta property="twitter:description" content="{!!$post->meta_description!!}">
<meta property="twitter:image" content="{{ asset('storage/'.$post->post_image)}}">


<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Article",
  "headline": "{{$post['title']}}",
  "image": "{{ asset('storage/'.$post->post_image)}}",  
  "author": {
    "@type": "Organization",
    "name": "Profectus Magazine"
  },  
  "publisher": {
    "@type": "Organization",
    "name": "Profectus Magazine",
    "logo": {
      "@type": "ImageObject",
      "url": ""
    }
  },
  "datePublished": "{{$post->created_at ? date( "d-M-Y h:i:sa", strtotime($post->created_at)) : ''}}",
  "description" : "{!!$post->meta_description!!}"
}
</script>
    
@endsection

<style>
  @media only screen and (max-width: 600px) {
    .col-md-12 {
      padding-right: 0px !important;
    padding-left: 0px !important;
  }
}
</style>
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=61a25cd9ab54b50012e51ed3&product=inline-share-buttons' async='async'></script>

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 blogDetailsWrap">
          <h2 class="blogHead" style="font-size:32px;margin-top: 20px;">{{$post->title}}</h2>
          <p>By : <span>{{$post->author_name}}</span>  &nbsp; / &nbsp; {{$post->created_at ? date( "d M Y", strtotime($post->created_at)) : ''}}  &nbsp;/ &nbsp; <span>{{$post->category_name}}</span></p>
          <!-- ShareThis BEGIN -->
<div class="sharethis-inline-share-buttons"></div>
<!-- ShareThis END -->
          <div class="blogImg" style="margin-top: 20px;">
            <img src="{{ asset('storage/'.$post->post_image)}}">
          </div>
          <div class="blogpara">
                {!!$post->content_html!!}
          </div>
         
        </div>
      </div>
      
    </div>
</div>
@include('client_logo_section')
@endsection
