@extends('layouts.main')
@section('page_title')
    <title>Profectus-Category</title>
@endsection
@section('content')


    <div class="newswrap">
        <h3>{{$category_name}}</h3>
    </div>
    <div class="container">

        <div class="cardwrap wrap_ent row">
            @if (count($postArr) > 0)
            @foreach ($postArr as $post)
                <div class="col-md-3 ">
                    <a href="/{{$post['slug']}}" target="_blank">
                    <div class="card shadow">
                        <div class="cardimg">
                            <img src="{{ asset('storage/'.$post['post_thumbnail'])}}">
                        </div>
                        <div class="cardInfo mt-3">
                            <strong class="greenTxt">{{$post['author_name']}}</strong>
                            <h5>{{$post['title']}}</h5>
                        </div>
                        <div class="cardData">
                            <p>{{$post['created_at'] ? date( "d M Y h:i A", strtotime($post['created_at'])) : ''}} </p>
                            <p>{{$post['category_name']}} > {{$post['sub_category_name']}} </p>
                        </div>
                    </div>
                    </a>
                </div>
            @endforeach
            @else
            <div class="container">
                <div class="col-md-12 text-center">
                    <h4 class="">No result found.</h4>
                </div>
            </div>
            @endif

        </div>
    @include('client_logo_section')
    </div>

@endsection
@section('scripts')
@endsection
