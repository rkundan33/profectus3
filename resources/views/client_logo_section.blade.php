@php
    $client_logo_arr = App\ClientLogo::where('is_active',1)->get()->toArray();
@endphp

<style>
    @media only screen and (max-width: 600px) {
    .clientsWrap img {
    width: 160px;
    padding-top: 24px;
    padding-left: 70px;
}
    }
    </style>


<div class="clientsWrap mt-5 row">
    @foreach ( $client_logo_arr as $cl)
        <div>
            <img class="respd" src="{{ asset('storage/'.$cl['path'])}}">
        </div>
    @endforeach

</div>
{{-- <div class="clientsWrap mt-5 row">
    <div>
        <img src="{{ asset('') }}assets/images/airtel.jpg">
    </div>
    <div>
        <img src="{{ asset('') }}assets/images/voda.jpg">
    </div>
    <div>
        <img src="{{ asset('') }}assets/images/rel.jpg">
    </div>
    <div>
        <img src="{{ asset('') }}assets/images/wip.jpg">
    </div>
    <div>
        <img src="{{ asset('') }}assets/images/tata.jpg">
    </div>
    <div>
        <img src="{{ asset('') }}assets/images/airtel.jpg">
    </div>
</div> --}}

