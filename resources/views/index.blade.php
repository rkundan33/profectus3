@extends('layouts.main')
@section('page_title')
    <title>Profectus-Homepage</title>
@endsection
<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="/assets/css/engine1/style.css" />
<script type="text/javascript" src="/assets/css/engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->
@section('content')
    <div class="bannerWrap">
        <!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
<div class="ws_images"><ul>
		<li><a href=""><img src="/assets/css/data1/images/Diggiple-1920-1280-1536x1024 (1).jpg" alt="javascript slideshow" id="wows1_0"/></a></li>
        <li><a href=""><img src="/assets/css/data1/images/Dinaz-Kalwachwala-1536x1024.jpg" alt="javascript slideshow" id="wows1_0"/></a></li>
        <li><a href=""><img src="/assets/css/data1/images/MohanDhanush1920-1280-1536x1024 (1).jpg" alt="javascript slideshow" id="wows1_0"/></a></li>
        <li><a href=""><img src="/assets/css/data1/images/Tizola-Founder-1920-1280-1536x1024.jpg" alt="javascript slideshow" id="wows1_0"/></a></li>
	</ul></div>
	
		
	</div></div>

</div>	
<script type="text/javascript" src="/assets/css/engine1/wowslider.js"></script>
<script type="text/javascript" src="/assets/css/engine1/script.js"></script>
<!-- End WOWSlider.com BODY section -->
    </div>
    <div class="container">
        <div class="cardwrap row mt-4">
            @foreach ($posts as $key => $post)
                <div class="col-md-3">
                    <a href="{{$post['slug']}}" target="_blank">
                        <div class="card shadow">
                            <div class="cardimg">
                                <img src="{{ asset('storage/'.$post['post_thumbnail'])}}">
                            </div>
                            <div class="cardInfo mt-3">
                                <strong class="greenTxt">{{$post['author_name']}}</strong>
                                <h5>{{$post['title']}}</h5>
                            </div>
                            <div class="cardData">
                                <p>{{$post['created_at'] ? date( "d M Y h:i A", strtotime($post['created_at'])) : ''}} </p>
                                <p>{{$post['category_name']}} > {{$post['sub_category_name']}} </p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>


        <div class="cardwrap row mt-4">
            <div class="col-md-12 text-center">
                <h3 class="pt-5 pb-0 textyellow">FUN FACTS</h3>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb2.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb1.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb4.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="entstoryWrap">
        <h3>Entreprenur Story</h3>
    </div>
    <div class="container">

        <div class="cardwrap wrap_ent row">
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb2.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb1.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb4.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-8">
                <div class="add1">
                    <img src="{{ asset('') }}assets/images/add.jpg">
                </div>
            </div>
            <div class="col-md-4">
                <div class="add1">
                    <img src="{{ asset('') }}assets/images/wb3.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="newswrap">
        <h3>NEWS AND UPDATES</h3>
    </div>
    <div class="container">

        <div class="cardwrap wrap_ent row">
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb2.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb1.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb4.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card shadow">
                    <div class="cardimg">
                        <img src="{{ asset('') }}assets/images/wb3.jpg">
                    </div>
                    <div class="cardInfo mt-3">
                        <strong class="greenTxt">Lorem Ipsum</strong>
                        <h5>India top businee - Lorem ipsum</h5>
                    </div>
                    <div class="cardData">
                        <p>MAR 2021 . 12 MINS READ . 1462 VIEWS</p>
                    </div>
                </div>
            </div>
        </div>
    @include('client_logo_section')
    </div>

@endsection
@section('scripts')
@endsection
