<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('page_title')
	<!-- General CSS Files -->
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/css/app.min.css">
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/jqvmap/dist/jqvmap.min.css">
	{{-- <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/weather-icon/css/weather-icons.min.css">
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/weather-icon/css/weather-icons-wind.min.css">
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/summernote/summernote-bs4.css"> --}}
	<!-- Template CSS -->
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/css/style.css">
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/css/components.css">
	<!-- Custom style CSS -->
	<link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/css/custom.css">
    <link rel="stylesheet" href="{{ asset('') }}admin_dashboard/assets/bundles/izitoast/css/iziToast.min.css">
    <link rel='shortcut icon' type='image/x-icon' href='{{ asset('') }}admin_dashboard/assets/img/favicon.ico' />
    <style>
        #overlay{
        position: fixed;
        top: 0;
        z-index: 100;
        width: 100%;
        height:100%;
        display: none;
        background: rgba(0,0,0,0.6);
        }
        .cv-spinner {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        }
        .spinner {
        width: 40px;
        height: 40px;
        border: 4px #ddd solid;
        border-top: 4px #2e93e6 solid;
        border-radius: 50%;
        animation: sp-anime 0.8s infinite linear;
        }
        @keyframes sp-anime {
        100% {
            transform: rotate(360deg);
        }
        }
        .is-hide{
        display:none;
        }
    </style>
    @yield('style_css')
</head>

<body>
	<div class="loader"></div>
    <div id="overlay">
        <div class="cv-spinner">
          <span class="spinner"></span>
        </div>
    </div>
	<div id="app">
		<div class="main-wrapper main-wrapper-1">
			<div class="navbar-bg"></div>
			<nav class="navbar navbar-expand-lg main-navbar">
				<div class="form-inline me-auto">
					<ul class="navbar-nav mr-3">
						<li>
							<a href="#" data-bs-toggle="sidebar" class="nav-link nav-link-lg
                            collapse-btn"> <i data-feather="align-justify"></i></a>
						</li>
						<li>
							<a href="#" class="nav-link nav-link-lg fullscreen-btn"> <i data-feather="maximize"></i> </a>
						</li>
						<li>
							<form class="form-inline me-auto">
								<div class="search-element d-flex">
									<input class="form-control" type="search" placeholder="Search" aria-label="Search">
									<button class="btn" type="submit"> <i class="fas fa-search"></i> </button>
								</div>
							</form>
						</li>
					</ul>
				</div>
				<ul class="navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" data-bs-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="{{ asset('') }}admin_dashboard/assets/img/users/user_icon.png" class="user-img-radious-style"> <span class="d-sm-none d-lg-inline-block"></span></a>
						<div class="dropdown-menu dropdown-menu-right pullDown">
							<div class="dropdown-title">Hello {{Auth::user()->name}}</div>
							{{-- <a href="#" class="dropdown-item has-icon"> <i class="far
										fa-user"></i> Profile </a> --}}
							<div class="dropdown-divider"></div>
							<a href="{{url('/logout')}}" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i> Logout </a>
						</div>
					</li>
				</ul>
			</nav>
			<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="/admin"> <img alt="image" src="{{ asset('') }}admin_dashboard/assets/img/text_admin_logo.png" class="header-logo" /> <span class="logo-name">Profectus</span> </a>
					</div>
					<div class="sidebar-user">
						<div class="sidebar-user-picture"> <img alt="image" src="{{ asset('') }}admin_dashboard/assets/img/users/user_icon.png"> </div>
						<div class="sidebar-user-details">
							<div class="user-name">{{Auth::user()->name}}</div>
							<div class="user-role">{{App\Role::where('role_id',Auth::user()->role_id)->first()->role_name}}</div>
						</div>
					</div>
					<ul class="sidebar-menu">
                        <li class="menu-header">Menu</li>
						<li class="@if(Request::is('admin')) active @endif"> <a href="/admin" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
						</li>
						<li class="@if(Request::is('admin/post') || Request::is('admin/new-post') || Request::is('admin/edit-post')) active @endif"> <a href="/admin/post" class="nav-link"><i data-feather="book-open"></i><span>Post</span></a>
						</li>
                        <li class="dropdown @if(Request::is('admin/category*') || Request::is('admin/subcategory*')) active @endif" >
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-level-down-alt fa-sm" style="width: 8px !important"></i>Category/Subcategory</a>
                            <ul class="dropdown-menu" style="display: none;">
                              <li><a href="/admin/category-list" class="nav-link">Category</a></li>
                              <li><a href="/admin/subcategory-list" >Sub Category</a></li>
                            </ul>
                        </li>

                        <li class="dropdown @if(Request::is('admin/website*')) active @endif">
                            <a href="#" class="nav-link has-dropdown"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-command"><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg><span>Webiste Element</span></a>
                            <ul class="dropdown-menu" style="display: none;">
                              <li><a href="/admin/website/logo" class="nav-link">Webiste Logo</a></li>
                              <li><a href="/admin/website/client-logo">Client Logo</a></li>
                            </ul>
                        </li>
                        @if (Auth::user()->role_id == 1)
                            <li class="@if(Request::is('admin/users-list')) active @endif  "> <a href="/admin/users-list" class="nav-link"><i data-feather="users"></i><span>Users</span></a>
                            </li>
                        @endif
                    </ul>
				</aside>
			</div> {{-- main content start here --}}
			<!-- Main Content -->
			<div class="main-content">
                @yield('content')
				<div class="settingSidebar">
					<a href="javascript:void(0)" class="settingPanelToggle"> <i class="fa fa-spin fa-cog"></i> </a>
					<div class="settingSidebar-body ps-container ps-theme-default">
						<div class=" fade show active">
							<div class="setting-panel-header">Setting Panel </div>
							<div class="p-15 border-bottom">
								<h6 class="font-medium m-b-10">Select Layout</h6>
								<div class="selectgroup layout-color w-50">
									<label class="selectgroup-item">
										<input type="radio" name="value" value="1" class="selectgroup-input-radio select-layout" checked> <span class="selectgroup-button">Light</span> </label>
									<label class="selectgroup-item">
										<input type="radio" name="value" value="2" class="selectgroup-input-radio select-layout"> <span class="selectgroup-button">Dark</span> </label>
								</div>
							</div>
							<div class="p-15 border-bottom">
								<h6 class="font-medium m-b-10">Sidebar Color</h6>
								<div class="selectgroup selectgroup-pills sidebar-color">
									<label class="selectgroup-item">
										<input type="radio" name="icon-input" value="1" class="selectgroup-input select-sidebar"> <span class="selectgroup-button selectgroup-button-icon" data-bs-toggle="tooltip" data-original-title="Light Sidebar"><i
                                                class="fas fa-sun"></i></span> </label>
									<label class="selectgroup-item">
										<input type="radio" name="icon-input" value="2" class="selectgroup-input select-sidebar" checked> <span class="selectgroup-button selectgroup-button-icon" data-bs-toggle="tooltip" data-original-title="Dark Sidebar"><i
                                                class="fas fa-moon"></i></span> </label>
								</div>
							</div>
							<div class="p-15 border-bottom">
								<h6 class="font-medium m-b-10">Color Theme</h6>
								<div class="theme-setting-options">
									<ul class="choose-theme list-unstyled mb-0">
										<li title="white" class="active">
											<div class="white"></div>
										</li>
										<li title="cyan">
											<div class="cyan"></div>
										</li>
										<li title="black">
											<div class="black"></div>
										</li>
										<li title="purple">
											<div class="purple"></div>
										</li>
										<li title="orange">
											<div class="orange"></div>
										</li>
										<li title="green">
											<div class="green"></div>
										</li>
										<li title="red">
											<div class="red"></div>
										</li>
									</ul>
								</div>
							</div>
							<div class="p-15 border-bottom">
								<div class="theme-setting-options">
									<label> <span class="control-label p-r-20">Mini Sidebar</span>
										<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" id="mini_sidebar_setting"> <span class="custom-switch-indicator"></span> </label>
								</div>
							</div>
							{{-- <div class="p-15 border-bottom">
								<div class="theme-setting-options">
									<div class="disk-server-setting m-b-20">
										<p>Disk Space</p>
										<div class="sidebar-progress">
											<div class="progress" data-height="5">
												<div class="progress-bar l-bg-green" role="progressbar" data-width="80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
											</div> <span class="progress-description">
                                                <small>26% remaining</small>
                                            </span> </div>
									</div>
									<div class="disk-server-setting">
										<p>Server Load</p>
										<div class="sidebar-progress">
											<div class="progress" data-height="5">
												<div class="progress-bar l-bg-orange" role="progressbar" data-width="58%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
											</div> <span class="progress-description">
                                                <small>Highly Loaded</small>
                                            </span> </div>
									</div>
								</div>
							</div> --}}
							<div class="mt-4 mb-4 p-3 align-center rt-sidebar-last-ele">
								<a href="#" class="btn btn-icon icon-left btn-primary btn-restore-theme"> <i class="fas fa-undo"></i> Restore Default </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="main-footer">
				<div class="footer-left"> Copyright &copy; {{date("Y")}}
					<div class="bullet"></div> Developed By <a href="#">Kundan Mandal</a> </div>
				<div class="footer-right"> </div>
			</footer>
		</div>
	</div>
	<!-- General JS Scripts -->

	<script src="{{ asset('') }}admin_dashboard/assets/js/app.min.js"></script>
	<!-- JS Libraies -->
	{{-- <script src="{{ asset('') }}admin_dashboard/assets/bundles/echart/echarts.js"></script> --}}
	{{-- <script src="{{ asset('') }}admin_dashboard/assets/bundles/chartjs/chart.min.js"></script> --}}
	<!-- Page Specific JS File -->
	{{-- <script src="{{ asset('') }}admin_dashboard/assets/js/page/index.js"></script> --}}
	<!-- Template JS File -->
	<script src="{{ asset('') }}admin_dashboard/assets/js/scripts.js"></script>
	<!-- Custom JS File -->
	<script src="{{ asset('') }}admin_dashboard/assets/js/custom.js"></script>
    <!-- JS Libraies -->
  <script src="{{ asset('') }}admin_dashboard/assets/bundles/izitoast/js/iziToast.min.js"></script>
  <!-- Page Specific JS File -->
    @yield('script')
    <script>
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>
</body>

</html>
