<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title', 'Profectus Magazine | Home')</title>
  @yield('meta')
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/main.css">
    <link rel="stylesheet" href="{{ asset('') }}assets/css/style.default.css" id="theme-stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lora:wght@600&display=swap" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('') }}assets/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('') }}assets/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('') }}assets/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('') }}assets/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('') }}assets/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('') }}assets/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('') }}assets/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('') }}assets/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('') }}assets/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('') }}assets/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('') }}assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('') }}assets/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('') }}assets/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('') }}assets/images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<style>
    .overlay {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: rgb(0,0,0);
  background-color: rgb(255 151 0 / 96%);
  overflow-x: hidden;
  transition: 0.5s;
}
p,h1,h2,h3,h4,h5,span{
  font-family: 'Lora', serif !important;
}

.overlay-content {
  position: relative;
  top: 14%;
  width: 100%;
  text-align: center;
  margin-top: 30px;
}

.overlay a {
  padding: 8px;
  text-decoration: none;
  font-size: 20px;
  color: #000;
  display: block;
  transition: 0.3s;
  font-weight: 700;
}

.overlay a:hover, .overlay a:focus {
  color: #f1f1f1;
}

.overlay .closebtn {
    position: absolute;
    top: -10px;
    right: 0px;
    font-size: 65px;
}

@media screen and (max-height: 450px) {
  .overlay a {font-size: 20px}
  .overlay .closebtn {
  font-size: 40px;
  top: 15px;
  right: 35px;
  }
  
}
@media only screen and (max-width: 600px) {
  #wowslider-container1 {
    position: static !important;
  }
  .mobilemenu{
    filter: drop-shadow(0px 1px 2px black);
    font-size: 30px;
    cursor: pointer;
    /* margin-top: -50px; */
    color: black;
    background-color: #ff9700;
    border-radius: 10px;
    padding-left: 10px;
    padding-right: 10px;
  }

}
@media only screen and (min-width: 600px) {
 .mobilemenu{
  display: none;
 }

}
  </style>
    @yield('style_css')
</head>
<body>

    <div class="loaderWrap"><img src="{{ asset('') }}assets/images/preload.gif"></div>
    <div class="top-bar">

      <div class="container-fluid">
        <div class="row d-flex align-items-center">
          <div class="col-lg-6 hidden-lg-down text-col">
            <ul class="list-inline">
              <li class="list-inline-item"><i class="icon-mail"></i><a href="mailto:SUPPORT@PROFECTUSMAGAZINE.COM">SUPPORT@PROFECTUSMAGAZINE.COM</a></li>

            </ul>
          </div>
          <div class="col-lg-6 d-flex justify-content-end">
            <!-- Language Dropdown-->
            <div class="dropdown show"> <a href="/contact/aboutus">About Us</a></div>
            <div class="dropdown show"> <a href="/crypto/market-capital">Contact Us</a></div>
            <div class="dropdown show"> <a href="https://rzp.io/l/Dmt7jvOj">Subscribe</a></div>


          </div>
        </div>
      </div>
    </div>
    <div class="logoWrap text-center">
        @include('website_logo_section')
    </div>
    <div class="container mb-5">
        <div class="col-md-12 col-12 p-0">
            <nav class="navbar navbar-expand-md">
              <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link search" id="searchbtn">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </a>
                   </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/"> <span>Home</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="#about"> <span>Health</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/"><span>Finance</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="#Benefits"><span>Awards</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="#Benefits"><span>Blogs</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="#Benefits"><span>Pride of the nation</span></a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="#reachus"><span>Entreprenur Stories</span></a>
                  </li>
                </ul>
              </div>
            </nav>
            <div class="searchWrap">
                <input type="search" class="form-control menusearch" >
                <input type="submit" class="search-submit" value="Search">
            </div>
          </div>
        </div>
        
        <div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
    <a href="#">Home</a>
    <a href="#">Awards</a>
    <a href="#">Health</a>
    <a href="#">Fashion</a>
    <a href="#">Finance</a>
    <a href="#">Blogs</a>
    <a href="#">Pride of the nation</a>
    <a href="#">Entreprenur Stories</a>
  </div>
</div>
<center style="margin-top: -20px;"><span class="mobilemenu" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></center>

    @yield('content')

    <div class="footer">
        <!-- Site footer -->
  <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
              <img src="https://test.profectusmagazine.com/storage/website_logo/1637485907_webiste_logo.png" style="width:220px;">

            <p class="text-justify">Each story we feature at Profectus comes with a perspective, a perspective of better tomorrow whether it be an individual or a company, whether it be social world or business, or of an average person, we at Profectus believe in telling stories that push you towards taking an initiative and making India bette with creative ideas and innovation.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Categories</h6>
            <ul class="footer-links">
              <li><a href="/">Awards</a></li>
              <li><a href="/">Fun Facts</a></li>
              <li><a href="/">Health</a></li>
              <li><a href="/">Fashion</a></li>
              <li><a href="/">Finance</a></li>
              <li><a href="/">Pride of the Nation</a></li>
              <li><a href="/">Entreprenur Stories</a></li>

            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="/contact/aboutus">About Us</a></li>
              <li><a href="/">Contact Us</a></li>
              <li><a href="https://rzp.io/l/Dmt7jvOj">Subscribe</a></li>
              <li><a href="/">Privacy Policy</a></li>
              <li><a href="/">Sitemap</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by
         <a href="#">Profectus</a>.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
</footer>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" type="text/javascript"></script>
  <script src="{{ asset('') }}assets/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="{{ asset('') }}assets/js/slick.js" type="text/javascript"></script>
  <script>
$('.lazy').slick({
dots: false,
arrow:true,
infinite: true,
fade: true,
speed: 1500,
slidesToShow: 1,
slidesToScroll: 1
});


if ($('.slick-slide').hasClass('slick-active')) {
  $('.sliderTxt').addClass('animated fadeInUp');
} else {
  $('.sliderTxt').removeClass('animated fadeInUp');
}

$(".lazy").on("beforeChange", function() {

  $('.sliderTxt').removeClass('animated fadeInUp').hide();
  setTimeout(() => {
    $('.sliderTxt').addClass('animated fadeInUp').show();

  }, 1000);

})

$("#searchbtn").click(function(){
  $(".searchWrap").slideToggle("slow");
  $(".searchWrap").css('display', 'flex')
});

$(".clientsWrap").slick({
  speed: 10000,
      autoplay: true,
      autoplaySpeed: 0,
      cssEase: 'linear',
  slidesToShow: 5,
    slidesToScroll: 1,
  infinite: true,
  swipeToSlide: true,
    centerMode: true,
  focusOnSelect: true,
  arrows:false,
  responsive: [
          {
            breakpoint: 750,
            settings: {
              slidesToShow: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
            }
          }
          ]
});

$('body').css('overflow-y', 'hidden');
setTimeout(function(){
    $('.loaderWrap').css('display', 'none');

     }, 3000);
     $( document ).ready(function() {
      setTimeout(function(){
    $('.loaderWrap').css('display', 'flex');
    $('body').css('overflow-y', 'scroll');
     }, 2000);
});
  </script>
  <!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
    window.__lc = window.__lc || {};
    window.__lc.license = 13219188;
    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>
<script>
function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>
<noscript><a href="https://www.livechatinc.com/chat-with/13219188/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->

  @yield('scripts')
</body>
</html>
