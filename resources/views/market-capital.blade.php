@extends('layouts.main')
@section('title')
@yield('title', 'Crypto Market Capital | Profectus Magazine')@endsection
@section('meta')
<!-- Primary Meta Tags -->

<meta name="title" content="Crypto Market Capital | Profectus Magazine">
<meta name="description" content="Crypto Market Capital">
<link rel="canonical" href="{{ Request::url()}}" />
<!-- Open Graph / Facebook -->
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article">
<meta property="og:url" content="{{ Request::url()}}">
<meta property="og:title" content="Crypto Market Capital via @ProfectusMagazine ">
<meta property="og:description" content="Crypto Market Capital">
<meta property="og:site_name" content="ProfectusMagazine" />
<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="{{ Request::url()}}">
<meta property="twitter:title" content="Crypto Market Capital | Profectus Magazine">
    
@endsection
@section('content')
<style>
  .tradingview-widget-copyright{
    display:none;
  }
  @media only screen and (min-width: 600px) {
  .tradingview-widget-container {
    padding-left:100px;
    padding-right:100px;
  }
  .mobp{
    display:none;
  }
}
@media only screen and (max-width: 600px) {
  .tradingview-widget-container {
    padding-left:5px;
    padding-right:5px;
  }
  
}
</style>
<center>
    <h3>Crypto Market Capital</h3><br>
    <p class="mobp">Please swipe to view more details of the crypto market capital</p>
<div class="tradingview-widget-container" style="border-radius: 20px;">
      <div class="tradingview-widget-container__widget"></div>
      
      <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-screener.js" async>
      {
        "width": "100%",
        "height": "500",
        "defaultColumn": "overview",
        "screener_type": "crypto_mkt",
        "displayCurrency": "USD",
        "colorTheme": "light",
        "locale": "in"
      }
      </script>
    </div>
</center>

@include('client_logo_section')
@endsection