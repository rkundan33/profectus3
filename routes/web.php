<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Post;
use App\Role;
use App\User;
use App\WebsiteLogo;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // dd(Auth::user());
    $data['posts'] = Post::select('posts.*','users.name as author_name','categories.name as category_name','sub_categories.name as sub_category_name')
                    ->where('is_draft',0)
                    ->leftJoin('categories','categories.id','posts.category_id')
                    ->leftJoin('sub_categories','sub_categories.id','posts.subcategory_id')
                    ->leftJoin('users','users.id','posts.author_id')
                    ->get()->toArray();
    return view('index',$data);
});
Route::get('/admin', function () {
    if(!Auth::user()){
        return redirect(route('login'));
    } else {
        $data['user_name'] = User::find(Auth::user()->id)->name;
        return view('Admin.index',$data);
    }
})->name('/admin');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

Route::get('{slug}', 'DashboardController@viewPost');
Route::get('/category/{cid}', 'DashboardController@categoryResult');
Route::get('/contact/aboutus', function () {
    // dd(Auth::user());
    return view('aboutus');
});
Route::get('/crypto/market-capital', function () {
    // dd(Auth::user());
    return view('market-capital');
});
//auth routes
Route::middleware(['auth'])->group(function () {

    //post



    Route::get('admin/post', 'DashboardController@postList')->name('admin/post');
    Route::get('admin/new-post', 'DashboardController@postCreate')->name('new-post');
    Route::post('admin/post-new-ajax', 'DashboardController@postCreateAjax');
    Route::get('admin/add-edit-post-content/{id}', 'DashboardController@addEditPostContent');
    Route::post('admin/add-edit-post-content-ajax', 'DashboardController@addEditPostContentAjax');

    Route::get('admin/post-edit/{id}', 'DashboardController@postEdit');
    Route::post('admin/delete-post', 'DashboardController@postDelete');

    // user
    Route::get('admin/users-list', 'DashboardController@usersList')->name('users-list');
    Route::post('admin/save-user-ajax', 'DashboardController@userSaveAjax');
    Route::post('admin/delete-user', 'DashboardController@userDelete');

    // webiste element
    Route::get('admin/website/logo', 'DashboardController@websiteLogo')->name('/website/logo');
    Route::post('admin/upload-webite-logo-ajax', 'DashboardController@uploadWebsiteLogoAjax');
    Route::post('admin/change-website-logo-status', 'DashboardController@changeWebsiteLogoStatus');

    Route::get('admin/category-list', 'DashboardController@categoryList');
    Route::post('admin/save-category-ajax', 'DashboardController@saveCategoryAjax');
    Route::post('admin/delete-category', 'DashboardController@deleteCategoryAjax');

    Route::get('admin/subcategory-list', 'DashboardController@subcategoryList');
    Route::post('admin/save-sub-category-ajax', 'DashboardController@saveSubCategoryAjax');
    Route::post('admin/delete-sub-category', 'DashboardController@deleteSubCategoryAjax');

    Route::get('admin/website/client-logo', 'DashboardController@clientLogo')->name('admin/website/client-logo');
    Route::post('admin/upload-client-logo-ajax', 'DashboardController@uploadClientLogoAjax');
    Route::post('admin/change-client-logo-status', 'DashboardController@changeClientLogoStatus');

    //subcategory
    Route::get('admin/get-subcategory-by-category-id','DashboardController@getSubcategory')->name('admin/get-subcategory-by-category-id');
});

